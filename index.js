const express = require("express");
const app = express();
require("dotenv").config();
const projectId = "newlea-lppc";
const sessionId = "123456";
const queries = ["агрессия"];
const languageCode = "ru";
const dialogflow = require("@google-cloud/dialogflow");
const sessionClient = new dialogflow.SessionsClient();
// these are not working:
// import {useRecoilState} from 'recoil'
// import {intentState} from './atoms/intentState.js'


async function detectIntent(
  projectId,
  sessionId,
  query,
  contexts,
  languageCode
) {
  // The path to identify the agent that owns the created intent.
  const sessionPath = sessionClient.projectAgentSessionPath(
    projectId,
    sessionId
  );

  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: query,
        languageCode: languageCode,
      },
    },
  };

  if (contexts && contexts.length > 0) {
    request.queryParams = {
      contexts: contexts,
    };
  }

  const responses = await sessionClient.detectIntent(request);
  return responses[0];
}

async function executeQueries(projectId, sessionId, queries, languageCode) {
  // Keeping the context across queries let's us simulate an ongoing conversation with the bot
  let context;
  let intentResponse;
  for (const query of queries) {
    try {
      console.log(`Sending Query: ${query}`);
      intentResponse = await detectIntent(
        projectId,
        sessionId,
        query,
        context,
        languageCode
      );
      const go=JSON.stringify(intentResponse.queryResult.intent.displayName)
      return(go)
    //   console.log(
    //     `Fulfillment Text: ${intentResponse.queryResult.fulfillmentText}`
    //   );
    //   // Use the context from this response for next queries
    //   context = intentResponse.queryResult.outputContexts;
    } catch (error) {
      console.log(error);
    }
  }
}

app.get("/", (req, res) => {
    executeQueries(projectId, sessionId, queries, languageCode).then((intentResolved)=>{
        console.log("THIS IS GREAT SUCCESS "+intentResolved);
        res.send(intentResolved)
    }).catch((err)=>{
        console.log(err);
    });
});
const port = process.env.PORT || 3232;
//process.env.PORT will be used when we deploy...
app.listen(port, () => {
  console.log("***server running on port***", port);
});
